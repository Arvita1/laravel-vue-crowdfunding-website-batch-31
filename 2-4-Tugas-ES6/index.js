//soal 1
let luasPersegipanjang = function (p, l) {
  return p * l;
};
let kelilingPersegipanjang = function (p, l) {
  return 2 * (p + l);
};
console.log("Function =", luasPersegipanjang(10, 5));
console.log("Function =", kelilingPersegipanjang(10, 5));

let luas = (p, l) => {
  return p * l;
};
let keliling = (p, l) => {
  return 2 * (p + l);
};
console.log("Arrow =", luas(10, 5));
console.log("Arrow =", keliling(10, 5));

//soal 2
const newFunction = function literal(firstName, lastName) {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function () {
      console.log(firstName + " " + lastName);
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();

let literal = (firstName, lastName) => {
  const john = `${firstName}  ${lastName}`;
  return john;
};
console.log(literal("William", "Imoh"));

//soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// console.log(firstName, lastName, address, hobby)

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)

let combinedArray = [...west,...east]
console.log(combinedArray)

//soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem  ${view}  dolor sit amet,  consectetur adipiscing elit, ${planet}` 
console.log(before)
console.log(after)

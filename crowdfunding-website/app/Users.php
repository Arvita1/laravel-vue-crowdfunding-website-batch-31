<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Users extends Model
{
    protected $fillable = ['username','email','name','role_id','email_verified_at','password','photo'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });

    }
    public function Roles()
    {
        return $this->hasOne(Roles::class);
    }
    public function OtpCodes()
    {
        return $this->hasOne(OtpCodes::class);
    }
   
}

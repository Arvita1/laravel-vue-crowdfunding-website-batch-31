<?php

namespace App\Listener;

use App\Event\userRegisteredMailEvent;
use App\Mail\userRegisteredMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailVerificationNotificationUserRegistered implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  userRegisteredMailEvent  $event
     * @return void
     */
    public function handle(userRegisteredMailEvent $event)
    {
        Mail::to($event->user)->send(new userRegisteredMail($event->user));
    }
}

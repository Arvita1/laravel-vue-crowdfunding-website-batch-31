<?php

namespace App\Listener;

use App\Event\userRegenerateOTPMailEvent;
use App\Mail\userRegenerateOTPMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailVerificationNotificationUserRegenerateOTP
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  userRegenerateOTPMailEvent  $event
     * @return void
     */
    public function handle(userRegenerateOTPMailEvent $event)
    {
        Mail::to($event->user)->send(new userRegenerateOTPMail($event->user));
    }
}

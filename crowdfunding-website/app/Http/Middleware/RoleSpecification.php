<?php

namespace App\Http\Middleware;

use App\Roles;
use Closure;
use Illuminate\Support\Facades\Auth ;

class RoleSpecification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user());
        if (Auth::user()->role_id == 'be7b12db-1bec-4e70-ac36-71a5e9ba25c3') {
            return $next($request);
        }
        else{
            return back();
        }
        // return $next($request);
        
    }
}

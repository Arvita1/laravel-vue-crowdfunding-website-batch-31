<?php

namespace App\Http\Middleware;

use App\Users;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerificationEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if ($request->user()->email_verified_at  != Null) {
            return $next($request);
        }
        else{
            return response()->json(
                [
                'response_code'=>"01",
                'response_message'=>'Email Belum diverifikasi'],
                200
                
            );
        }
        
    }
}

<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'Profile berhasil ditampilkan',
            'data'=>auth()->user()],
            200
        );
    }
    
    public function UpdateProfile(Request $request)
    {
        $updateProfile  = Users::find(auth()->user()->id);
        $request->validate([
            'name'=> 'required',
            'photo' => 'required'
        ]);
        $updateProfile->name = request('name');
        $updateProfile->photo = request('photo');
        $updateProfile->save();
        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'Profile berhasil dirubah',
            'data'=>$updateProfile],
            200
        );
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCodes;
use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon as SupportCarbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $otpCode = OtpCodes::where('otp',$request->otp)->first();
        if (!$otpCode) {
            return response()->json(
                [
                'response_code'=>"01",
                'response_message'=>'OTP tidak ditemukan'],
                200
            );
        }
        if (Carbon::now() > $otpCode->valid_until) {
            return response()->json(
                [
                'response_code'=>"01",
                'response_message'=>'OTP tidak berlaku silakan generate ulang'],
                200
            );
        }

        $user = Users::find($otpCode->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();
        $otpCode->delete();
        $data['user']= $user;
        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'verifikasi berhasil',
            'data'=>$data],
            200
        );
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Event\userRegenerateOTPMailEvent;
use App\Event\userRegisteredMailEvent;
use App\Http\Controllers\Controller;
use App\Mail\userRegenerateOTPMail;
use App\OtpCodes;
use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegenerateOTPController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $getUser = Users::where('email',$request->email)->first();
        $generate = OtpCodes::where('user_id',$getUser->id)->first();
        $generate->otp=  Str::random(6);
        $generate->valid_until = Carbon::now()->addMinutes(5);
        $generate->save();
        $user = (object)[
            'email' => $getUser['email'],
            'otp' => $generate['otp'],
        ];
        
        event(new userRegenerateOTPMailEvent($user));
        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'berhasil regeneret otp, check your mail',
            'data'=>$getUser],
            200
        );
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Event\userRegisteredMailEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\userRegisteredMail;
use App\OtpCodes;
use App\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $data = Users::create([
            'name'=> request('name'),
            'username' => request('username'),
            'email'=> request('email'),
            'password'=> bcrypt(request('password')),
        ]);
        $otp = OtpCodes::create([
            'user_id'=> $data->id,
            'otp' => Str::random(6),
            'valid_until'=> Carbon::now()->addMinutes(5),
        ]);
        $user = (object)[
            'email' => $data['email'],
            'otp' => $otp['otp'],
        ];
        
        event(new userRegisteredMailEvent($user));

        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'you are registerd, check your mail',
            'data'=>$data],
            200
        );
    }
}

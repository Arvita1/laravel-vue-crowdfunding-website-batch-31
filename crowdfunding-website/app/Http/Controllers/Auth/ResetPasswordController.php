<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Users;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $reset = Users::where('email',$request->email)->first();
        if (!$reset) {
            return response()->json(
                [
                'response_code'=>"01",
                'response_message'=>'email tidak ditemukan'],
                200
            );
        }
        $reset->password = bcrypt(request('password'));
        if (request('password')==request('password_confirmation')) {
            $reset->save();
        } else {
            return response()->json(
                [
                'response_code'=>"01",
                'response_message'=>'password confirmasi salah',
                200
                ]
            );
        }
        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'kamu berhasil reset password',
            'data'=>$reset],
            200
        );
        
        
    }
}

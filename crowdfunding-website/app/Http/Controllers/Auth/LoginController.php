<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email'=> 'required',
            'password' => 'required'
        ]);
        // $token =auth()->attempt($request->only('email','password'));
        if (!$token= auth()->attempt($request->only('email','password'))  ) {
            return response(null,401);
        }
        
        // return response()->json(compact('token'));
        return response()->json(
            [
            'response_code'=>"00",
            'response_message'=>'Berhasil Login',
            'token'=> $token,
            'user' => auth()->user()
        ],
            200
        );
    }
}

<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function random($count)
    {
        $blogs = Blog::select('*')
        ->inRandomOrder()
        ->limit($count)
        ->get();
        $data['blogs']=$blogs;
        return response()->json(
            [
                'response_code'=>"00",
                'response_message'=>'data blogs berhasil di tampilkan',
                'data'=>$data],
            200
        );
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' =>'required',
            'description' => 'required',
            // 'image' => 'required|mimes:jpg,jpeg,png',
        ]);
        $blogs =Blog::create([
            'title'=>$request->title,
            'description'=>$request->description,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $blogs->id.".".$image_extension;
            $image_folder ="/photos/campaign";
            $image_location = $image_folder.$image_name;
            try {
                $image->save(public_path($image_folder), $image_name);
                $blogs->update(['image'=>$image_location]);
            } catch (\Throwable $th) {
                return response()->json(
                    [
                    'response_code'=>"01",
                    'response_message'=>'photo gagal upload'],
                    200
                );
            }
            $data['campaign']= $blogs;
            return response()->json(
                [
                'response_code'=>"00",
                'response_message'=>'data blogs berhasil di tambahkan',
                ],
                200
            );
        }
    }
}

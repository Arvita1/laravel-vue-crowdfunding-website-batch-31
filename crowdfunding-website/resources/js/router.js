import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode:'history',
    routes:[
        {
            path:'/',
            name:'home',
            alias:'/home',
            component: ()=>import('./views/Home.vue')
        },
        {
            path:'/donation',
            name:'donation',
            component: ()=>import('./views/Donations.vue')
        },        
        {
            path:'/blog',
            name:'blog',
            component: ()=>import('./views/Blogs.vue')
        },
        {
            path:'*',
            redirect:'/'
        },
    ]
});

export default router
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->group(function () {
    Route::post('register','RegisterController'); 
    Route::post('login','LoginController'); 
    Route::post('verification','VerificationController'); 
    Route::post('regenerate-otp','RegenerateOTPController'); 
    Route::post('update-password','ResetPasswordController'); 
});

Route::middleware('verification_email')->group(function(){
    Route::get('profile', 'ProfileController');
    Route::post('update-profile', 'ProfileController@UpdateProfile');
});

Route::group([
    'middleware'=>'api',
    'prefix' => 'campaign',
], function(){
    Route::get('random/{count}','CampaignController@random');
    Route::post('store','CampaignController@store');
});

Route::group([
    'middleware'=>'api',
    'prefix' => 'blog',
], function(){
    Route::get('random/{count}','BlogController@random');
    Route::post('store','BlogController@store');
});
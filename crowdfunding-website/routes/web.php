<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('app');
// });

// Route::get('/route-1', 'HomeController@route1')->middleware('verification_email');
// Route::get('/route-2', 'HomeController@route2')->middleware('verification_email','role_specification');
// // });

// Route::middleware('verification_email')->group(function(){
//     Route::get('/route-1', 'HomeController@route1');
//     Route::get('/route-2', 'HomeController@route2')->middleware('role_specification');
// });

Route::view('/{any?}', 'app')->where('any','.*');
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campaigns')->insert([
            'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'title' => Str::random(10),
            'description' => Str::random(10),
        ]);
    }
}

<?php 
abstract class Hewan {

    public $nama;
    public $darah=50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        $nama = $this->nama;
        $keahlian = $this->keahlian;
        echo $nama." sedang ".$keahlian;
        echo "<br>";
    } 
    public function getDarah(){
        return $this->darah;
    }
    public function setDarah($darah){
        $this->darah = $darah;
    }
    
}

abstract class Fight extends Hewan
{
    public $attackPower;
    public $defencePower;

    public function serang($a, $b, $attackPower, $defencePower)
    {
        echo  $a." sedang menyerang ".$b;
        echo "<br>";
        $darah = $this->getDarah();
        $darah = $this->diserang($b, $darah, $attackPower, $defencePower);
        echo "<br>";
        return $darah;
    }

    public function diserang( $a, $darah, $attackPower, $defencePower)
    {
        echo  $a." sedang diserang ";
        echo "<br>";
        $darah = ($darah-$attackPower/$defencePower);
        echo "Darah ".$a." = ".$darah."<br>";
        return $darah;

    }
    

}

class Harimau  extends Fight 
{
    public function getInfoHewan()
    {
        echo "<br>";
        echo "Hewan = " .$this->nama . " <br>"; 
        echo "Jumlah kaki = " .$this->jumlahKaki . " <br>"; 
        echo "Keahlian = " .$this->keahlian . " <br>"; 
        echo "Attack Power = " .$this->attackPower . " <br>"; 
        echo "Defence Power = " .$this->defencePower . " <br>"; 
    }

}

class Elang extends Fight 
{
    public function getInfoHewan()
    {
        echo "<br>";
        echo "Hewan = " .$this->nama . " <br>"; 
        echo "Jumlah kaki = " .$this->jumlahKaki . " <br>"; 
        echo "Keahlian = " .$this->keahlian . " <br>"; 
        echo "Attack Power = " .$this->attackPower . " <br>"; 
        echo "Defence Power = " .$this->defencePower . " <br>"; 
    }
}

$harimau = new Harimau();
$harimau->nama = "harimau";
$harimau->jumlahKaki = 4;
$harimau->keahlian = "jalan cepat";
$harimau->attackPower = 7;
$harimau->defencePower = 8;
$elang = new Elang();
$elang->nama = "elang";
$elang->jumlahKaki = 2;
$elang->keahlian = "terbang tinggi";
$elang->attackPower = 10;
$elang->defencePower = 5;
$harimau->atraksi();
$elang->atraksi();
echo "<br>";
echo "Darah ".$harimau->nama ." = ".$harimau->getDarah(). " VS Darah ".$elang->nama . "= ".$elang->getDarah() ."<br>";
$darah = $harimau->serang($harimau->nama,$elang->nama, $harimau->attackPower, $elang->defencePower);

$elang->setDarah($darah);
echo "Darah ".$harimau->nama ." = ".$harimau->getDarah(). " VS Darah ".$elang->nama . "= ".$elang->getDarah() ."<br>";
$darah = $harimau->getDarah();
$darah = $harimau->diserang($harimau->nama, $darah,$elang->attackPower,$harimau->defencePower);
echo "<br>";
$harimau->setDarah($darah);
echo "Darah ".$harimau->nama ." = ".$harimau->getDarah(). " VS Darah ".$elang->nama . "= ".$elang->getDarah() ."<br>";
$harimau->getInfoHewan();
$elang->getInfoHewan();

?>
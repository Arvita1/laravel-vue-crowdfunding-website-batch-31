//soal 1
daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
console.log(daftarHewan)

//soal 2
function introduce(data) {
    var perkenalan = "Nama saya "+data.name+", umur saya "+data.age+" tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu "+data.hobby+"!";
  return perkenalan;
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 



//soal 3
function hitung_huruf_vokal(str) {
    var hitung_huruf_vokal = 0;
  
    for (var i = 0; i < str.length; i++) {
  
      if (str[i] == "a" || str[i] == "e" || str[i] == "i" ||
        str[i] == "o" || str[i] == "u" || str[i] == " " ||  str[i] == "A" 
        || str[i] == "E" || str[i] == "I" ||  str[i] == "O" ||  str[i] == "U") {
            hitung_huruf_vokal++;
      }
    }
    return (hitung_huruf_vokal);
  }

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//Soal 4
function hitung(hitung) {
    var data = [-2,0,2,4,6,8]
    return data[hitung]
  }
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8